=======================
Youtube Playlist to MPD
=======================

.. image:: screenshot.jpg

----
TODO
----

* DONE Not reload on change/prev/next song
* Generate API doc
* Generate frontend doc

-----
Intro
-----

Raspberry PI + youtube as music player

------------
Technologies
------------

* node.js
* mpd
* youtube
* html + js

----
Libs
----

Express
=======

API Server


node-ytdl-core
==============

Fetch youtube song info

mpd node package
================

Connect to mpd server/service

Mithril
=======

HTML client logic

Bulma
=====

HTML client look & feel
