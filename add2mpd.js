#!/usr/bin/env node

const miniget = require('miniget')
const ytdl    = require('ytdl-core')
const mpd     = require('mpd')

// eslint-disable-next-line no-console
const _l = console.log.bind(console)

let options = require('./options.json')

const URL_LIST = 'https://www.youtube.com/list_ajax?style=json&action_get_list=1&list='

const cmd = mpd.cmd
let client = mpd.connect({
  host: options.mpd.host,
  port: options.mpd.port
})

if (process.argv.length < 3) {
  _l('Example: add2mpd.js RDEMK96mLEwrO1xH5CK40wg4hA')
  process.exit(1)
}

let id = process.argv[2]
if (id.length < 12) {
  id = 'RD' + id
}

client.on('ready', () => {
  miniget(URL_LIST + id, (err, r, body) => {
    _l('ready body list')
    let data = {}
    try {
      data = JSON.parse(body)
    } catch(e) {
      _l(e)
      process.exit(2)
    }
    const queue = data.video.map((video, i) => {
      return {video, i}
    })
    processQueue(queue)
  })
})

function addVideo(video, i, cb) {
  _l('fetching', video.encrypted_id)
  ytdl.getInfo(video.encrypted_id, (err, info) => {
    if (err) {
      _l(err)
      process.exit(3)
    }
    _l('adding', video.encrypted_id)
    const format = ytdl.chooseFormat(info.formats, { quality: 'highestaudio' })
    const duration = video.duration
    const title = info.title.replace(/\s/g, '_') + '_(' + duration + ')'
    const url = format.url + '&_' + title + ';;;' + video.encrypted_id
    client.sendCommand(cmd('add', [url]))
    if (i == 0) {
      client.sendCommand(cmd('play', []))
    }
    cb()
  })
}

function processQueue(queue) {
  if (!queue.length) {
    close()
    return
  }
  let data = queue.shift()
  addVideo(data.video, data.i, () => {processQueue(queue)})
}

function close() {
  client.sendCommand(cmd('close', []))
}
