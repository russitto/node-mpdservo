const express  = require('express')
const bodypa   = require('body-parser')
const miniget  = require('miniget')
const ytdl     = require('ytdl-core')
const mpd      = require('mpd')

let options = require('./options.json')

const cmd    = mpd.cmd
const app    = express()
let client = mpd.connect({
  host: options.mpd.host,
  port: options.mpd.port
})

// eslint-disable-next-line no-console
const _l = console.log.bind(console)
// eslint-disable-next-line no-console
const _e = console.error.bind(console)

const URL_LIST = 'https://www.youtube.com/list_ajax?style=json&action_get_list=1&list='

app.use(bodypa.json())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Range')
  next()
})

app.get('/yt/:id', ytInfo)
app.get('/ytlist/:id', ytList)

app.get('/clear', (req, res) => simpleCmd(req, res, 'clear'))
app.get('/pause', (req, res) => simpleCmd(req, res, 'pause'))
app.get('/prev', prev)
app.get('/next', next)
app.get('/volup', volup)
app.get('/voldown', voldown)
app.get('/seek/:pos', seek)
app.get('/options', getOptions)
app.post('/options', setOptions)

app.get('/playlist', playlist)
app.get(/\/_(.+);;;(.+)/, proxyMusic)

app.post('/list', list)

module.exports = { cmd, app, client }

client.on('ready', () => {
  _l('mpd connection ready')
  client.sendCommand(cmd('play', []))
  getSongs().then(songs => _l(songs))
  getStatus().then(status => _l(status))
})

/* send websocket message with status + songs
client.on('system-player', function() {
  client.sendCommand(cmd("status", []), function(err, msg) {
    if (err) throw err
    console.log(msg)
  })
})
*/

function ytInfo(req, res) {
  ytdl.getInfo(req.params.id, (err, info) => {
    if (err) {
      res.statusCode = 500
      return res.end(err)
    }
    res.set('Content-Type', 'application/json')
    const format = ytdl.chooseFormat(info.formats, { quality: 'highestaudio' })
    res.json({
      title: info.title,
      length: parseInt(info.length_seconds),
      type: format.type,
      encoding: format.audioEncoding,
      url: format.url
    })
  })
}

function ytList (req, res) {
  res.set('Content-Type', 'application/json')
  miniget(URL_LIST + req.params.id, (err, r, body) => {
    if (err) {
      res.statusCode = 500
      _e(err)
      return res.end('ERR')
    }
    res.end(body)
  })
}

function list(req, res) {
  // client.sendCommand(cmd('clear', []))
  // const urls = req.body.map(song => song.id)
  const queue = req.body.map((song, i) => {
    return {id: song.id, i}
  })
  processQueue(queue)
  res.json('OK')
}

function simpleCmd(req, res, thacmd) {
  client.sendCommand(cmd(thacmd, []))
  res.json('OK')
}

function playlist(req, res) {
  Promise.all([getSongs(), getStatus()])
    .then(([songs, status]) => res.json({songs, status}))
}

function getSongs() {
  return new Promise((resolve, reject) => {
    client.sendCommand(cmd('playlist', []), (err, msg) => {
      if (err) {
        return reject(err)
      }
      const reg0 = /\d+:file:\s?/
      // eslint-disable-next-line no-useless-escape
      const reg1 = /http(s)?:\/\/[a-zA-Z0-9_\-\.]+(:\d+)?\/api\//
      const songs = msg.split('\n').map(s => s.replace(reg0, '').replace(reg1, ''))
      resolve(songs)
    })
  })
}

function getStatus() {
  return new Promise((resolve, reject) => {
    client.sendCommand(cmd('status', []), (err, msg) => {
      if (err) {
        return reject(err)
      }
      resolve(msg2status(msg))
    })
  })
}

function msg2status(msg) {
  let obj = {}
  msg.split('\n').forEach(m => {
    m = m.trim()
    if (m != '') {
      let params = m.split(':')
      const id = params.shift().trim()
      let val = params.join(':').trim()
      if (!isNaN(val)) {
        val = parseFloat(val)
      }
      obj[id] = val
    }
  })
  return obj
}

function seek(req, res) {
  const pos = parseInt(req.params.pos)
  cmdAndStatus(cmd('seek', [pos, 0]), res)
}
function prev(req, res) {
  cmdAndStatus(cmd('previous', []), res)
}
function next(req, res) {
  cmdAndStatus(cmd('next', []), res)
}

function cmdAndStatus(command, res) {
  res.set('Content-Type', 'application/json')
  client.sendCommands([command, cmd('status', [])], (err, msg) => {
    res.json(msg2status(msg))
  })
}

function volup(req, res) {
  setVolume(res, 5)
}
function voldown(req, res) {
  setVolume(res, -5)
}
function setVolume(res, inc) {
  res.set('Content-Type', 'application/json')
  getStatus().then(status => {
    let volume = status.volume + inc
    if (volume < 0) volume = 0
    if (volume > 100) volume = 100
    res.json(volume)
    client.sendCommand(cmd('setvol', [volume]))
  })
}

function proxyMusic(req, res) {
  ytdl.getInfo(req.params[1], (err, info) => {
    if (err) {
      res.statusCode = 500
      _e(err)
      return res.end('mmm')
    }
    res.set('Content-Type', 'application/json')
    const format = ytdl.chooseFormat(info.formats, { quality: 'highestaudio' })
    miniget(format.url).pipe(res)
  })
}

function mpdReconn(host, port) {
  client.sendCommand(cmd('close',[]))
  client = mpd.connect({ host, port })
}

function setOptions(req, res) {
  options = Object.assign(options, req.body)
  if (req.body.mpd) {
    const host = req.body.mpd.host || 'localhost'
    const port = parseInt(req.body.mpd.port) || 6600
    mpdReconn(host, port)
  }
  res.json(options)
}

function getOptions(req, res) {
  res.json(options)
}

function addVideo(id, i, cb) {
  ytdl.getInfo(id, (err, info) => {
    if (err) {
      return _e(err)
    }
    const format = ytdl.chooseFormat(info.formats, { quality: 'highestaudio' })
    const duration = minsAndSecs(info.length_seconds)
    const title = info.title.replace(/\s/g, '_') + '_(' + duration + ')'
    const url = format.url + '&_' + title + ';;;' + id
    client.sendCommand(cmd('add', [url]))
    if (i == 0) {
      client.sendCommand(cmd('play', []))
    }
    cb()
  })
}

function processQueue(queue) {
  if (!queue.length) return
  let data = queue.shift()
  addVideo(data.id, data.i, () => {processQueue(queue)})
}

function minsAndSecs(duration) {
  duration = parseInt(duration)
  const mins = parseInt(duration / 60)
  let secs = duration % 60
  if (secs < 10) secs = '0' + secs
  return `${mins}:${secs}`
}
