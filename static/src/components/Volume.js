import u from '../_utils'

export default class Volume {
  oninit(vnode) {
    this.state = { volume: formatVolume(vnode.attrs.status) }
  }

  view() {
    return m('.buttons.has-addons.is-centered', [
      m('a.button[href="/voldown"]', { onclick: this.setVolume.bind(this) }, '⇓'),
      m('button#volLabel.button[disabled]', {
        onclick: ev => ev.preventDefault()
      }, 'Volume: ' + this.state.volume),
      m('a.button[href="/volup"]', { onclick: this.setVolume.bind(this) }, '⇑')
    ])
  }

  setVolume(ev) {
    ev.preventDefault()
    const url = ev.target.pathname
    return m.request(u.base + url).then(volume => {
      this.state.volume = formatVolume(volume)
    })
  }
}

function formatVolume(volume) {
  if (volume == 100) volume = 99
  if (volume < 10) volume = '0' + volume
  return volume
}
